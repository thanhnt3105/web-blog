import React, { useEffect, useState } from "react";
import {
	Box,
	Heading,
	Link,
	Image,
	Text,
	Container,
	Divider,
	Grid,
	GridItem,
	IconButton,
} from "@chakra-ui/react";
import { motion } from "framer-motion";
import { Link as routerLink, useParams } from "react-router-dom";
import { db } from "../../lib/firebase";
import { doc, onSnapshot } from "firebase/firestore";
import { AiOutlineRollback } from "react-icons/ai";
import { ROOT } from "../../lib/routes";
import Navbar from "./Navbar";
import Comment from "./Comment";
import CommentInput from "./CommentInput";
import { useAuth } from "../../hooks/auths";
import {
	useAddComment,
	useGetComment,
	useDeleteComment,
} from "../../hooks/comment";
import dayjs from "dayjs";
import ReactHtmlParser from "react-html-parser";

export default function CurrentPost() {
	const { postId } = useParams();
	const [currentPost, setCurrentPost] = useState([]);
	const { user, authLoading } = useAuth();
	const { addComment, isLoading } = useAddComment();
	const { commentData } = useGetComment(postId);
	const [commentsData, setCommentsData] = useState([]);
	const { deleteComment } = useDeleteComment();

	useEffect(() => {
		if (commentData.length > 0) {
			setCommentsData(commentData);
		}
	}, [commentData]);
	const handleAddComment = (commentText) => {
		const _data = commentsData;
		const dataPush = {
			content: commentText,
			postId,
			timeCreated: dayjs().toString(),
			user,
		};
		_data.push(dataPush);
		setCommentsData(_data);
		addComment(currentPost.id, commentText, user.id);
	};

	const handleDelete = (commentId) => {
		const _data = commentsData.filter((data) => data.id !== commentId);
		setCommentsData(_data);
		deleteComment(commentId);
	};
	useEffect(() => {
		onSnapshot(doc(db, "posts", postId), (snapshot) => {
			setCurrentPost({ ...snapshot.data(), id: snapshot.id });
		});
	}, []);

	console.log(currentPost);

	return (
		<>
			<Navbar />
			<motion.div layout>
				<Container maxW={"7xl"} p="12">
					<motion.button
						whileHover={{
							scale: 1.2,
							transition: { duration: 1 },
						}}
						whileTap={{ scale: 0.9 }}
					>
						<IconButton
							colorScheme="#319594"
							as={routerLink}
							to={ROOT}
							size="lg"
							icon={<AiOutlineRollback />}
							isRound
							variant="ghost"
						/>
					</motion.button>
					<Heading as="h2">{currentPost.title}</Heading>

					<Divider marginTop="5" />
					<Grid
						templateColumns="repeat(auto-fill, minmax(100%, 1fr))"
						gap={6}
						marginTop="5"
					>
						<GridItem>
							<Box w="100%">
								<Box borderRadius="lg" overflow="hidden">
									<Link
										textDecoration="none"
										_hover={{ textDecoration: "none" }}
									>
										<Image
											transform="scale(1.0)"
											src={currentPost.imageUrl}
											alt="blog image here"
											width="100%"
											height={"60vh"}
											objectFit="cover"
											transition="0.3s ease-in-out"
											_hover={{
												transform: "scale(1.05)",
											}}
										/>
									</Link>
								</Box>
								<Text as="p" fontSize="md" marginTop="8">
									{/* {currentPost.desc} */}
									{ReactHtmlParser(currentPost.desc)}
								</Text>
								<Text as="p" fontSize="md" marginTop="8">
									{/* {currentPost.desc} */}
									{ReactHtmlParser(currentPost.content)}
								</Text>
							</Box>
						</GridItem>
					</Grid>
					<Text fontSize={"24px"} py={4}>
						COMMENTS
					</Text>
					{commentsData.map((comment) => (
						<div key={comment.id} style={{ position: "relative" }}>
							<Comment
								username={comment?.user?.username}
								content={comment.content}
								avatarUrl={comment.avatarUrl}
								timestamp={dayjs(comment.timestamp).format(
									"YYYY-MM-DD HH:mm:ss"
								)}
							/>
							{currentPost.uid === user.id && (
								<button
									style={{ position: "absolute", top: "4px", right: "8px" }}
									onClick={() => handleDelete(comment.id)}
								>
									X
								</button>
							)}
						</div>
					))}
					<div>
						<CommentInput user={user} onSubmit={handleAddComment} />
					</div>
				</Container>
			</motion.div>
		</>
	);
}
