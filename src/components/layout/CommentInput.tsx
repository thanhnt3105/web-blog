import React, { useState } from "react";
import { Box, Avatar, Input, Button } from "@chakra-ui/react";

const CommentInput = ({ onSubmit, user }) => {
	const [commentText, setCommentText] = useState("");

	const handleInputChange = (e) => {
		setCommentText(e.target.value);
	};

	const handleSubmit = () => {
		// Call the onSubmit callback with the new comment text
		onSubmit(commentText);

		// Reset the comment input after submission
		setCommentText("");
	};

	return (
		<Box
			display="flex"
			alignItems="center"
			p={4}
			borderRadius="md"
			boxShadow="md"
		>
			<Avatar size="sm" name={user?.username} mr={4} />
			<Input
				type="text"
				placeholder="Add a comment..."
				value={commentText}
				onChange={handleInputChange}
				flex="1"
			/>
			<Button ml={4} colorScheme="blue" onClick={handleSubmit}>
				Submit
			</Button>
		</Box>
	);
};

export default CommentInput;
