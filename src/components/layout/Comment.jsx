import React from "react";
import { Box, Avatar, Text } from "@chakra-ui/react";

const Comment = ({ username, content, timestamp }) => {
	return (
		<Box
			display="flex"
			alignItems="center"
			p={4}
			borderRadius="md"
			boxShadow="md"
		>
			<Box>
				<Box display="flex" alignItems="center">
					<Avatar size="sm" name={username} mr={4} />
					<Text fontSize="sm" fontWeight="bold">
						{username}
					</Text>
				</Box>
				<Text fontSize="sm">{content}</Text>
				<Text fontSize="xs" color="gray.500">
					{timestamp}
				</Text>
			</Box>
		</Box>
	);
};

export default Comment;
