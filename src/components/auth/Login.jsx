import {
	Flex,
	Box,
	FormControl,
	FormLabel,
	Input,
	Stack,
	Link,
	Button,
	Heading,
	Text,
	useColorModeValue,
	FormErrorMessage,
	InputGroup,
	InputRightElement,
	HStack,
	Image,
} from "@chakra-ui/react";
import { useState } from "react";
import { REGISTER, ROOT } from "../../lib/routes";
import { Link as routerLink } from "react-router-dom";
import { useForm } from "react-hook-form";
import { emailValidate, passwordValidate } from "../../utils/form-validation";
import { useLogin, signInWithGoogle} from "../../hooks/auths";
import LoginPic from "../../assets/login.png";
import GoogleIcon from "../../assets/google-icon.svg";
// import { GoogleLogin, Google} from 'react-google-login';
import { useGoogleLogin} from "../../hooks/auths";


export default function Login() {
	const [show, setShow] = useState(false);
	const { login, isLoading } = useLogin();
	const { googleLogin, isGoogleLoading} = useGoogleLogin();

	const {
		register,
		handleSubmit,
		reset,
		formState: { errors },
	} = useForm();

	async function handleLogin(data) {
		const succeeded = await login({
			email: data.email,
			password: data.password,
			redirectTo: ROOT,
		});
		if (succeeded) reset();
	}



	const handleClick = () => setShow(!show);
	return (
		<Flex
			minH={"100vh"}
			align={"center"}
			justify={"center"}
			bg={useColorModeValue("gray.50", "gray.800")}
		>
			<HStack>
				<Box boxSize="lg">
					<Image src={LoginPic} alt="Dan Abramov" />
				</Box>
				<Stack spacing={8} mx={"auto"} maxW={"lg"} minW={500} py={12} px={6}>
					<Stack align={"center"}>
						<Heading fontSize={"4xl"}>Website Blog</Heading>
						<Text fontSize={"lg"} color={"gray.600"}>
							More knowledge, more money 💵
						</Text>
					</Stack>
					<Box
						rounded={"lg"}
						bg={useColorModeValue("white", "gray.700")}
						boxShadow={"lg"}
						p={8}
					>
						<Stack spacing={4}>
							<form onSubmit={handleSubmit(handleLogin)}>
								<FormControl id="email" isInvalid={errors.email}>
									<FormLabel>Email</FormLabel>
									<Input
										type="text"
										placeholder="user@email.com"
										{...register("email", emailValidate)}
									/>
									<FormErrorMessage>
										{errors.email && errors.email.message}
									</FormErrorMessage>
								</FormControl>
								<FormControl id="password" mb={3} isInvalid={errors.email}>
									<FormLabel>Password</FormLabel>
									<InputGroup size="md">
										<Input
											placeholder="Password123"
											type={show ? "text" : "password"}
											{...register("password", passwordValidate)}
										/>
										<InputRightElement width="4.5rem">
											<Button h="1.75rem" size="sm" onClick={handleClick}>
												{show ? "Hide" : "Show"}
											</Button>
										</InputRightElement>
										<FormErrorMessage>
											{errors.password && errors.password.message}
										</FormErrorMessage>
									</InputGroup>
								</FormControl>
								<Stack>
									<Stack
										direction={{ base: "column", sm: "row" }}
										align={"end"}
										justify={"end"}
									>
										<Link as={routerLink} to={ROOT} color={"blue.400"}>
											Explore blogs?
										</Link>
									</Stack>
									<Button
										bg={"blue.400"}
										color={"white"}
										_hover={{
											bg: "blue.500",
										}}
										type="submit"
										isLoading={isLoading}
										loadingText="Logging In"
									>
										Sign in
									</Button>

									<Text fontSize="sm" align={"center"}>
										or
									</Text>
									<Button
										bg={"#c946a2d9"}
										color={"white"}
										_hover={{
											bg: "#95126fe8",
										}}
										
										isLoading={isGoogleLoading}
										loadingText="Logging In"
										onClick={() => googleLogin({ redirectTo: ROOT })}
									>
										<img
											style={{ height: "28px", marginRight: "4px" }}
											src={GoogleIcon}
											alt="Google"
										/>
										Sign in with Google
									</Button>
								</Stack>
							</form>
							<Stack pt={6}>
								<Text align={"center"}>
									You don't have account?{" "}
									<Link color={"blue.400"} as={routerLink} to={REGISTER}>
										Register now!
									</Link>
								</Text>
							</Stack>
						</Stack>
					</Box>
				</Stack>
			</HStack>
		</Flex>
	);
}
