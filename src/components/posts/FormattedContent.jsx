import React from "react";
import ReactHtmlParser from "react-html-parser";

const FormattedContent = ({ content }) => {
  return <>{ReactHtmlParser(content)}</>;
};

export default FormattedContent;
