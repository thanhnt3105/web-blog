import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    Input,
    Stack,
    Link,
    Button,
    Heading,
    Text,
    Textarea,
    useColorModeValue,
    FormHelperText,
    useToast,
    ButtonGroup,
    
  } from "@chakra-ui/react";
  import TextareaAutosize from "react-textarea-autosize";
  import {useForm} from "react-hook-form";
  import {useAddPost} from "../../hooks/posts";
  import {useAuth} from "../../hooks/auths";
  import { useEffect, useRef, useState } from "react";
  import { RiArrowRightSLine } from "react-icons/ri";
  // import { BiBold, BiItalic, BiText, BiTextSize } from "react-icons/bi";
  
  import { BiBold, BiItalic, BiText } from "react-icons/bi";
  import { IoText } from "react-icons/io5";
  
  
  export default function SimpleCard({onModalClose}) {
    const {addPost, isLoading} = useAddPost();
    const {user, authLoading} = useAuth();
  
    const [isBold, setIsBold] = useState(false);
    const [isItalic, setIsItalic] = useState(false);
    const [isLargeText, setIsLargeText] = useState(false);
    const [fontSize, setFontSize] = useState(14); // Set the default font size (e.g., 14px)
  
    const textAreaRef = useRef();
  
    const {
      register,
      handleSubmit,
      reset,
      formState: {errors},
    } = useForm();
    const toast = useToast();
  
    // const handleBold = () => setIsBold(!isBold);
    // const handleItalic = () => setIsItalic(!isItalic);
    // const handleFontSize = () => {
    //   Tùy chỉnh font size theo nhu cầu của bạn
    //   const newSize = fontSize === 16 ? 20 : 16;
    //   setFontSize(newSize);
    // };
  
    const handleBold = () => {
      setIsBold(!isBold);
    };
    
    const handleItalic = () => {
      setIsItalic(!isItalic);
    };
    
    const handleFontSize = () => {
      setIsLargeText(!isLargeText);
      setFontSize(isLargeText ? 16 : 14); 
    };
    
  
    const applyFormat = (format) => {
      const textArea = textAreaRef.current;
      const start = textArea.selectionStart;
      const end = textArea.selectionEnd;
      const selectedText = textArea.value.slice(start, end);
  
      let formattedText;
      switch (format) {
        case "bold":
          formattedText = isBold ? `<strong>${selectedText}</strong>` : selectedText;
          break;
        case "italic":
          formattedText = isItalic ? `<em>${selectedText}</em>` : selectedText;
          break;
        default:
          formattedText = selectedText;
          break;
      }
  
      const newText = `${textArea.value.slice(0, start)}${formattedText}${textArea.value.slice(end)}`;
      textArea.value = newText;
    };
  
    const handleAddPost = data => {
  
         // Xử lý việc thêm định dạng vào mô tả bài đăng
         const formattedDesc = textAreaRef.current.value;
  
      addPost({
        uid: user.id,
        title: data.title,
        desc: data.desc,
        imageUrl: data.imageUrl,
      });
      reset();
      onModalClose();
    };
  
    return (
      <Flex minH={"40vh"} align={"center"} justify={"center"}>
        <Stack spacing={8} mx={"auto"} maxW={"90%"} minW={"90%"} py={12}>
          <Stack align={"center"}>
            <Heading fontSize={"4xl"}>Add new post</Heading>
          </Stack>
          <Box
            rounded={"lg"}
            bg={useColorModeValue("white", "gray.700")}
            boxShadow={"lg"}
            p={8}
          >
            <Stack>
              <form onSubmit={handleSubmit(handleAddPost)}>
                <FormControl id='title'>
                  <FormLabel>Blog Title</FormLabel>
                  <Input
                    type='text'
                    {...register("title", {required: true, maxLength: 120})}
                  />
                  <FormHelperText>
                    Eg: The Art of Effective Communication
                  </FormHelperText>
                </FormControl>
                <FormControl id='image'>
                  <FormLabel> Image URL</FormLabel>
                  <Input type='url' {...register("imageUrl", {required: true})} />
                  <FormHelperText>
                    <Link
                      onClick={() => {
                        navigator.clipboard.writeText(
                          "https://picsum.photos/200/300/"
                        );
                        toast({
                          title: "URL Copied",
                          status: "success",
                          isClosable: true,
                          position: "top",
                          duration: 2000,
                        });
                      }}
                    >
                      Eg: https://picsum.photos/200/300/
                    </Link>
                    <div>
                      <Text as='mark'>Copy image link by click</Text>
                    </div>
                  </FormHelperText>
                </FormControl>
  
                <FormControl id='desc'>
                  <FormLabel> Description</FormLabel>
                    <ButtonGroup mt={2}>
                      <Button
                        size="sm"
                        onClick={handleBold}
                        fontWeight={isBold ? 'bold' : 'normal'}
                      >
                        <BiBold />
                      </Button>
                      <Button
                        size="sm"
                        onClick={handleItalic}
                        fontStyle={isItalic ? 'italic' : 'normal'}
                      >
                        <BiItalic />
                      </Button>
                      <Button
                        size="sm"
                        onClick={handleFontSize}
                        style={{ fontSize: fontSize + 'px', fontWeight: isBold ? 'bold' : 'normal', fontStyle: isItalic ? 'italic' : 'normal' }}
                      >
                        <BiText />
                      </Button>
                      {/* <Button size="sm" onClick={() => applyFormat('bold')}>
                        <BiText />
                      </Button> */}
                      {/* Add other buttons for different formatting options if needed */}
                    </ButtonGroup>
                  <Textarea
                    ref={textAreaRef}
                    placeholder='I know writing can be tough, Just type "blah blah blah" to test things out!'
                    as={TextareaAutosize}
                    minRows={5}
                    resize={"none"}
                    style={{ fontSize: fontSize + "px", fontWeight: isBold ? "bold" : "normal", fontStyle: isItalic ? "italic" : "normal" }}
                    {...register("desc", {required: true})}
                  />
  
                  
  
                </FormControl>
                <Stack spacing={10}>
                  <Button
                    mt={"10px"}
                    bg={"blue.400"}
                    color={"white"}
                    _hover={{
                      bg: "blue.500",
                    }}
                    type='submit'
                    isLoading={isLoading}
                    loadingText={"Loading..."}
                  >
                    Hit the Big Blue Button! POST
                  </Button>
                </Stack>
              </form>
            </Stack>
          </Box>
        </Stack>
      </Flex>
    );
  }
  