import { useToast } from "@chakra-ui/react";
import {
	arrayRemove,
	arrayUnion,
	collection,
	doc,
	orderBy,
	query,
	getDocs,
	setDoc,
	getDoc,
	updateDoc,
	addDoc,
	deleteDoc,
	where,
	serverTimestamp,
} from "firebase/firestore";
import { useState, useEffect } from "react";
import { db } from "../lib/firebase";
import * as dayjs from "dayjs";
import {
	useCollectionData,
	useDocumentData,
} from "react-firebase-hooks/firestore";
import { comment } from "postcss";

export function useAddComment() {
	const [isLoading, setLoading] = useState(false);

	async function addComment(postId, content, userId) {
		setLoading(true);
		try {
			const commentsRef = collection(db, "comments");

			// Add a new comment document to the "comments" collection
			const newCommentRef = await addDoc(commentsRef, {
				postId: postId,
				content: content,
				userId: userId,
				timesCreated: dayjs().toString(),
			});

			// Get the ID of the newly created comment document
			const commentId = newCommentRef.id;

			// Update the "comments" field in the "posts" document with the new comment's ID
			const postRef = doc(db, "posts", postId);
			await updateDoc(postRef, {
				comments: arrayUnion(commentId),
			});
			console.log("Comment added successfully!");
			setLoading(false);
		} catch (error) {
			console.error("Error adding comment:", error);
			setLoading(false);
		}
	}

	return { addComment, isLoading };
}

export function useGetComment(postId) {
	const [commentData, setCommentData] = useState([]);
	const [isLoading, setLoading] = useState(false);
	const [userLoading, setUserLoading] = useState(false);
	const [userError, setUserError] = useState(null);

	useEffect(() => {
		// Call getComments to fetch the comments data
		async function getComments() {
			setLoading(true);
			try {
				const postRef = doc(db, "posts", postId);
				const postDoc = await getDoc(postRef);

				if (postDoc.exists()) {
					// Retrieve the array of comment IDs from the post document's "comments" field
					const commentIds = postDoc.data().comments || [];

					// Fetch the content of comments based on the comment IDs
					const commentsData = await Promise.all(
						commentIds.map(async (commentId) => {
							const commentRef = doc(db, "comments", commentId);
							const commentDoc = await getDoc(commentRef);
							if (commentDoc.exists()) {
								const commentData = commentDoc.data();

								// Retrieve the user information based on the "userId" field
								const userId = commentData.userId;

								// Fetch user data from Firestore
								try {
									setUserLoading(true);
									const userRef = doc(db, "users", userId);
									const userDoc = await getDoc(userRef);
									if (userDoc.exists()) {
										const userData = userDoc.data();
										// Include the user information in the comment data
										return { id: commentId, ...commentData, user: userData };
									} else {
										console.error(`User with ID ${userId} not found`);
									}
								} catch (error) {
									console.error(
										`Error fetching user with ID ${userId}:`,
										error
									);
									setUserError(`Error fetching user with ID ${userId}`);
								} finally {
									setUserLoading(false);
								}
							}
							return null;
						})
					);

					// Filter out any null values (in case a comment document was deleted)
					const filteredComments = commentsData.filter(
						(comment) => comment !== null
					);

					setCommentData(filteredComments);
				} else {
					console.error("Post not found");
				}
				setLoading(false);
			} catch (error) {
				console.error("Error fetching comments:", error);
				setLoading(false);
			}
		}

		getComments();
	}, [postId]);

	return { commentData, isLoading, userLoading, userError };
}

export function useDeleteComment() {
	const toast = useToast();
	const [isLoading, setLoading] = useState(false);

	async function deleteComment(commentId) {
		setLoading(true);
		try {
			const commentRef = doc(db, "comments", commentId);
			await deleteDoc(commentRef);
			setLoading(false);
			toast({
				title: "Delete Comment Successfully",
				status: "success",
				isClosable: true,
				position: "top",
				duration: 5000,
			});
		} catch (error) {
			console.error("Error deleting comment:", error);
			setLoading(false);
		}
	}

	return { deleteComment, isLoading };
}
